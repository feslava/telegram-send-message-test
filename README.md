# README #
Simple Android App that tests the Telegram TDLib (Telegram Database library) sending messages to your Telegram Contacts.

This code use:
    
1: Telegram TDLib library. Source code has been downloaded from https://github.com/tdlib/td.
    
2: Basic library example. Example class is located at https://github.com/tdlib/td/tree/master/example/java (copyright Aliaksei Levin).

### License ###
TDLib is licensed under the terms of the Boost Software License. See TDLib home page for more information.

### What is this repository for? ###
Andoid
Version v0.0.1

### How do I get set up? ###
To run it, you should fill in the following MainActivity class attributes with your personal values:
    
    public static int APP__ID = 0;
    
    public static String APP__HASH = ""; 
    
    public static String APP__PHONE_NUMBER = ""; 
    
    public static String APP__AUTHENTICATION_CODE = "";

That's all!!
