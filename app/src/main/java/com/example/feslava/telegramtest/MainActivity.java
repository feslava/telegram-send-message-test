package com.example.feslava.telegramtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    public static int APP__ID = 0; // to fill with your app id
    public static String APP__HASH = ""; // to fill with your app hash
    public static String APP__PHONE_NUMBER = ""; // to fill with your phone number
    public static String APP__AUTHENTICATION_CODE = ""; // to fill with your authentication code

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        // HISTORY scroll
        TextView textview = findViewById(R.id.history_txt);
        textview.setMovementMethod(new ScrollingMovementMethod());

        // SEND button
        findViewById(R.id.send_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText contactEditText = findViewById(R.id.contact_txt);
                EditText messageEditText = findViewById(R.id.message_txt);
                if ((contactEditText != null) && (messageEditText != null)) {
                    String contact = contactEditText.getText().toString();
                    String message = messageEditText.getText().toString();
                    if ((contact.length() > 0) && (message.length() > 0)) {
                        sendMessage(contact, message);
                    }
                }
            }
        });

    }

    private boolean sendMessage(String contact, String message) {
        try {
            Example example = new Example(MainActivity.this);
            if (example.sendMessage(contact, message)) {
                TextView history =  findViewById(R.id.history_txt);
                if (history != null) {
                    // Adds message to history
                    String time = "[" + getCurrentTime() + "]";
                    history.append(time + "\nSent to " + contact + ": " + message + "\n\n");
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

    private String getCurrentTime()
    {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR) + " " +
                formatTimeValue(calendar.get(Calendar.MONTH)) + " " +
                formatTimeValue(calendar.get(Calendar.DAY_OF_MONTH)) + " " +
                formatTimeValue(calendar.get(Calendar.HOUR_OF_DAY)) + ":" +
                formatTimeValue(calendar.get(Calendar.MINUTE)) + ":" +
                formatTimeValue(calendar.get(Calendar.SECOND));
    }

    private String formatTimeValue(int value) {
        NumberFormat f = new DecimalFormat("00");
        return String.valueOf(f.format(value));
    }

}
